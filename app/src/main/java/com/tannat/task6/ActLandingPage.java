package com.tannat.task6;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class ActLandingPage extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_landing_page);
        initViews();
    }

    private void initViews() {
        findViewById(R.id.btn_create_acc).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_create_acc:
                Intent intent = new Intent();
                intent.setClass(this, FrgIntroduction.class);
                startActivity(intent);
                break;
            default:
                break;
        }
    }
}
